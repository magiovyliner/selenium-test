package utilities;

import org.apache.logging.log4j.*;

public class Log {

	private static Logger log = LogManager.getLogger(Log.class.getName());
	
	public static void startTestCase(String tcName){		 
		log.info("=== " + tcName + " ===");	 
	}
	
	public static void endTestCase(String tcName){		 
		log.info("=== End of TestCase " + tcName + " ===");	 
	}
	
	public static void info(String message){
		 log.info(message);
	}
		 
	public static void warn(String message) {
		 log.warn(message);
	}
	
	public static void error(String message) {
		 log.error(message);
	}
	
	public static void fatal(String message) {
		 log.fatal(message);
	}
	
	public static void debug(String message) {
		 log.debug(message);
	}
}
