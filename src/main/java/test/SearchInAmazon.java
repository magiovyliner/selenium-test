package test;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
//import org.seleniumhq.jetty9.xml.XmlAppendable;
//import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pageFactory.amazonHomePage;
import utilities.Log;

public class SearchInAmazon {

	WebDriver driver;
	amazonHomePage objHomePage;
	
	@BeforeTest
	public void setup(){
		
		Log.startTestCase("Test_001");
		
		driver = new FirefoxDriver();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		
		// 1. Go to https://www.amazon.com
		driver.get("https://www.amazon.com/");
	}
	
	@Test(priority=0)
	public void test_HomePage_Search(){
		objHomePage = new amazonHomePage(driver);
		
		// 2. In the search area, input "iPhone".
		objHomePage.searchForProduct("iPhone");
		objHomePage.clickSearchButton();
		
		// 3. Refine the results to show only items under the "Cell Phones" department.
		objHomePage.refineByDepartment("Cell Phones");
		
		// 4. Refine results to show only items ranging from $400 ~ $500.
		objHomePage.refineByMinPrice(400);
		objHomePage.refineByMaxPrice(500);
		objHomePage.clickRefineByPrice();
		
		// 5. Take the first 5 results then sort their prices by highest to lowest in Java, 
		// without using 'Sort by Price' feature on Amazon's website.
		objHomePage.takeNumberOfResults(5);
		objHomePage.sortAccordingToPrice();
		
		// 6. Print all the product names after sorting.
		ArrayList<String> productNames = objHomePage.getProductNames();
		objHomePage.printList(productNames, "Printing product names from search criteria");
	}
	
	@AfterTest
	public void teardown(){
		Log.endTestCase("Test_001");
		driver.quit();
	}
}
