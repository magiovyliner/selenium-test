package pageFactory;

import java.util.ArrayList;
import java.util.Arrays;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import utilities.Log;

public class amazonHomePage {
	
	WebDriver driver;
	WebElement[] results;
	ArrayList<String> productNames = new ArrayList<String>();
	
	@FindBy(xpath=".//form[@class='nav-searchbar']//input[@name='field-keywords']")
	WebElement searchInput;
	
	@FindBy(xpath=".//form[@class='nav-searchbar']//input[@type='submit']")
	WebElement searchButton;
	
	@FindBy(xpath=".//input[@id='low-price']")
	WebElement minPriceInput;
	
	@FindBy(xpath=".//input[@id='high-price']")
	WebElement maxPriceInput;
	
	@FindBy(xpath=".//div[@id='priceRefinements']//input[@type='submit']")
	WebElement priceRefineButton;
	
	@FindBy(xpath=".//div[contains(@class,'s-result-list')]/div")
	WebElement[] listOfResults;
			
	String departmentSectionEl = ".//div[@id='departments']//span[text()='%s']";
	String priceEl = "//span[@class='a-price']/span[1]";
	String productNameEl = ".//div[contains(@class,'s-result-list')]/div//h2//span";
	
	public amazonHomePage(WebDriver driver){		
		this.driver = driver;
		PageFactory.initElements(driver, this);		
	}
	
	public void searchForProduct(String product){
		searchInput.sendKeys(product);
	}
	
	public void clickSearchButton(){
		searchButton.click();
	}
	
	private WebElement departmentLink(String department) {
		  return (WebElement) By.xpath(String.format(departmentSectionEl, department));
	}
	
	public void refineByDepartment(String department){
		departmentLink(department).click();
	}
	
	public void refineByMinPrice(double minPrice){
		minPriceInput.sendKeys(Double.toString(minPrice));
	}
	
	public void refineByMaxPrice(double maxPrice){
		maxPriceInput.sendKeys(Double.toString(maxPrice));
	}
	
	public void clickRefineByPrice(){
		priceRefineButton.click();
	}
	
	public void takeNumberOfResults(int n){
		results = Arrays.copyOfRange(listOfResults, 0, n+1);
	}
	
	public void sortAccordingToPrice(){
		double cur, next;
		WebElement temp;
		for (int i = 0; i < results.length; i++)
		{
			for(int j = 0; j < results.length; j++)
		    {
				cur = sanitizePrice(results[i].findElement(By.xpath(priceEl)).getText());
				next = sanitizePrice(results[j+1].findElement(By.xpath(priceEl)).getText());
				
                if(cur < next)
                {
                    temp = results [j + 1];
                    results [j + 1]= results [i];
                    results [i] = temp;
                }
	       }
		}
	}
	
	private double sanitizePrice(String strPrice){
		int pos = strPrice.indexOf("&nbsp;");
		return Double.parseDouble(strPrice.replace(",", "").substring(pos));
	}
	
	public ArrayList<String> getProductNames(){		
		for(int i=0; i < results.length; i++){
			productNames.add(results[i].findElement(By.xpath(productNameEl)).getText());
		}
		return productNames;
	}
	
	public void printList(ArrayList<String> list, String message){
		Log.info(message);
		for(String item : list){
			Log.info(item);
		}
	}
}
